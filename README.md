# oscilloscope

Some random python code to use OWON VDS6102

## Status

Can be used as a library to fetch data and as a program to show the waveform of oscilloscope

## Help wanted!

- Documentation!
- Commands, which are not used in `display_process.py` are not tested!
- Setup as a package

## Known issues!

1. Apparently, `:WAV:DATA?` returns only channel 1 waveforms!

   - Workarounds: use `:WAV:FETCH?`

2. `:WAV:DATA?` time step is different from `:WAV:FETCH?`, so be careful!
   - Solution: see [`test.ipynb`](test.ipynb)
