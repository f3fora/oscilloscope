def config(inst):
    # inst.function.set("SINE")
    # inst.function_frequency.set(1e3)  # Hz
    # inst.function_offset.set(1.65)  # V
    # inst.function_amplitude.set(2)  # Vpp

    inst.trigger_single_edge_level.set(3)  # div
    inst.trigger_single_edge_source.set("CH1")

    inst.horizontal_scale.set("5ms")
    inst.ch1_scale.set("500mv")
    inst.ch1_offset.set(-5)

    inst.ch2_scale.set("500mv")
    inst.ch2_offset.set(-5)

    inst.ch1_display.set("ON")
    inst.ch2_display.set("ON")
