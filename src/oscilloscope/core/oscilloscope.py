from itertools import chain
from pathlib import Path

import toml

from .oscilloscope_properties import (
    OscilloscopeDataProperty,
    OscilloscopePreambleProperty,
    OscilloscopeRawDataProperty,
)
from .properties import Property

__commands_file = Path(__file__).parent / "oscilloscope_commands.toml"


def __merge_commands(dictionary):
    return list(chain(*list(dictionary.values())))


def __replace_channels(commands, channels):
    good, bad = [], []
    for c in commands:
        (good if "<n>" in c["cmd"] else bad).append(c)

    ch_commands = [[]] * channels
    for c in good:
        for i, g in enumerate(ch_commands):
            aus = c.copy()
            aus["cmd"] = c["cmd"].replace("<n>", str(i + 1))
            if "name" in c:
                aus["cmd"] = c["cmd"].replace("<n>", str(i + 1))

            g.append(aus)

    l = list(chain(*ch_commands))

    return l + bad


def __replace_measure(commands, measures, pattern="<m>"):
    good, bad = [], []
    for c in commands:
        (good if pattern in c["cmd"] else bad).append(c)

    ch_commands = [[]] * len(measures)
    for c in good:
        for m, g in zip(measures, ch_commands):
            aus = c.copy()
            aus["cmd"] = c["cmd"].replace(pattern, m)
            if "name" in c:
                aus["cmd"] = c["cmd"].replace(pattern, m)

            g.append(aus)

    l = list(chain(*ch_commands))

    return l + bad


__parsed_file = toml.load(__commands_file)
__info = __parsed_file.pop("info")
__type = __parsed_file.pop("type_measures")

_channels = __info["channels"]

__parsed_file["oscilloscope_ch"] = __replace_channels(
    __parsed_file["oscilloscope_ch"], _channels
)

__parsed_file["measure"] = __replace_measure(
    __parsed_file["measure"], __type["single_channel_measure"], "<sm>"
)

__parsed_file["measure"] = __replace_measure(
    __parsed_file["measure"], __type["two_channels_measure"], "<tm>"
)

_commands = __merge_commands(__parsed_file)
model = __info["idn"]


class Instrument:
    def __init__(self, instrument, commands):
        self._instrument = instrument

        for command in commands:
            if not "name" in command:
                command["name"] = command["cmd"][1:].replace(":", "_").lower()

            setattr(
                self,
                command.pop("name"),
                Property(**command, instrument=self._instrument),
            )


class Oscilloscope(Instrument):
    def __init__(self, instrument, commands=_commands, channels=_channels):
        super().__init__(instrument, commands)
        self.channels = channels

        self.waveform_data.conversion = OscilloscopeDataProperty

        self.waveform_preamble.conversion = OscilloscopePreambleProperty

        self.waveform_fetch.conversion = OscilloscopeRawDataProperty
