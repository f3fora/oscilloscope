import numpy as np


def at(o, n):
    return slice(o, o + n)


def h2dig(bytes_string, typ=np.uint8, ords="low"):
    """
    this is actually a 4 bit uint
    """
    bits = np.unpackbits(
        np.frombuffer(bytes_string, np.dtype(typ).newbyteorder(ords)), bitorder=ords
    )
    return bits.reshape(4, -1) @ np.array([1, 2, 4, 8])


def h2n(bytes_string, n, typ=np.uint8, ords="low"):
    """
    return the first n bit
    """
    bits = np.unpackbits(
        np.frombuffer(bytes_string, np.dtype(typ).newbyteorder(ords)), bitorder=ords
    )
    return bits[:n]


def uint2(bytes_string, typ=np.uint16, ords="low"):
    return np.frombuffer(bytes_string, np.dtype(typ).newbyteorder(ords)).squeeze()


def uint4(bytes_string, typ=np.uint32, ords="low"):
    return np.frombuffer(bytes_string, np.dtype(typ).newbyteorder(ords)).squeeze()


def int2(bytes_string, typ=np.int16, ords="low"):
    return np.frombuffer(bytes_string, np.dtype(typ).newbyteorder(ords)).squeeze()


def int4(bytes_string, typ=np.int32, ords="<"):
    return np.frombuffer(bytes_string, np.dtype(typ).newbyteorder(ords)).squeeze()


def float4(bytes_string, typ=np.float32, ords="low"):
    return np.frombuffer(bytes_string, np.dtype(typ).newbyteorder(ords)).squeeze()


def hexl(bytes_string):
    return bytes_string[::-1].hex()


class OscilloscopeWrongFormat(Exception):
    pass


class OscilloscopeProperty:
    voltage_scale = np.array(  # [V]
        [
            1e-3,
            2e-3,
            5e-3,
            10e-3,
            20e-3,
            50e-3,
            100e-3,
            200e-3,
            500e-3,
            1.0,
            2.0,
            5.0,
        ]
    )

    time_scale = np.array(  # [ns]
        [
            1.0,
            2.0,
            5.0,
            10.0,
            20.0,
            50.0,
            100.0,
            200.0,
            500.0,
            1e3,
            2e3,
            5e3,
            10e3,
            20e3,
            50e3,
            100e3,
            200e3,
            500e3,
            1e6,
            2e6,
            5e6,
            10e6,
            20e6,
            50e6,
            100e6,
            200e6,
            500e6,
            1.0e9,
            2.0e9,
            5.0e9,
            10.0e9,
            20.0e9,
            50.0e9,
            100.0e9,
        ]
    )

    def __init__(self, data):
        self.data = data
        ini = chr(self.data[0])
        if ini != "#":
            raise OscilloscopeWrongFormat()

        n = int(chr(self.data[1]))
        self.d = int(self.data[at(2, n)])
        if len(self.data[2 + n :]) != self.d:
            raise OscilloscopeWrongFormat()

        last = self.data[-self.d :]

        if hexl(last[at(0, 8)]) != "090906060A0A0550".lower():
            raise OscilloscopeWrongFormat()

        self.dyn_check = uint2(last[at(8, 2)])
        self.N1 = uint2(last[at(10, 2)])
        self.running_status = uint2(last[at(12, 2)])
        self.avaiable_vertical_resolution = uint2(last[at(14, 2)])
        self.n1 = uint2(last[at(16, 2)])
        self.n2 = uint4(last[at(18, 4)])
        self.n3 = uint2(last[at(22, 2)])
        self.N2 = self.n3 * self.n1 * (self.n2 * 2 + 2)
        self.n4 = uint2(last[at(24, 2)])
        self.n5 = uint4(last[at(26, 4)])
        self.N3 = self.n4 * (self.n5 * 2 + 2)
        self.waveform_forming_method = uint2(last[at(30, 2)])
        self.normal_scan = uint2(last[at(32, 2)])
        self.no_scrolling_data_in_scan_mode = uint4(last[at(34, 4)])

        self.frequency_counter_count = np.empty(4, dtype=np.int32)
        self.frequency_counter_count[0] = int4(last[at(38, 4)])
        self.frequency_counter_count[1] = int4(last[at(42, 4)])
        self.frequency_counter_count[2] = int4(last[at(46, 4)])
        self.frequency_counter_count[3] = int4(last[at(50, 4)])

        self.frequency_counter_reference_count = np.empty(4, dtype=np.int32)
        self.frequency_counter_reference_count[0] = int4(last[at(54, 4)])
        self.frequency_counter_reference_count[1] = int4(last[at(58, 4)])
        self.frequency_counter_reference_count[2] = int4(last[at(62, 4)])
        self.frequency_counter_reference_count[3] = int4(last[at(66, 4)])

        self.overflow_flag = h2n(last[at(70, 2)], 4)

        self.adc_minimum = np.empty(4, dtype=np.int16)
        self.adc_minimum[0] = int2(last[at(72, 2)])
        self.adc_minimum[1] = int2(last[at(74, 2)])
        self.adc_minimum[2] = int2(last[at(76, 2)])
        self.adc_minimum[3] = int2(last[at(78, 2)])

        self.adc_maximum = np.empty(4, dtype=np.int16)
        self.adc_maximum[0] = int2(last[at(80, 2)])
        self.adc_maximum[1] = int2(last[at(82, 2)])
        self.adc_maximum[2] = int2(last[at(84, 2)])
        self.adc_maximum[3] = int2(last[at(86, 2)])

        self.adc_average = np.empty(4, dtype=np.int16)
        self.adc_average[0] = int2(last[at(88, 2)])
        self.adc_average[1] = int2(last[at(90, 2)])
        self.adc_average[2] = int2(last[at(92, 2)])
        self.adc_average[3] = int2(last[at(94, 2)])

        self.channel_trigger_time = h2dig(last[at(96, 2)])

        self.system_clock = int2(last[at(98, 2)])

        self.data_acquisition_sync_ID = last[at(256, 4)]

        self.acquired_voltage_index = np.empty(4, dtype=np.uint16)
        self.acquired_voltage_index[0] = uint2(last[at(260, 2)])
        self.acquired_voltage_index[1] = uint2(last[at(262, 2)])
        self.acquired_voltage_index[2] = uint2(last[at(264, 2)])
        self.acquired_voltage_index[3] = uint2(last[at(266, 2)])

        self.acquired_zero = np.empty(4, dtype=np.float32)
        self.acquired_zero[0] = float4(last[at(268, 4)])
        self.acquired_zero[1] = float4(last[at(272, 4)])
        self.acquired_zero[2] = float4(last[at(276, 4)])
        self.acquired_zero[3] = float4(last[at(280, 4)])

        self.channel_working_status = h2dig(last[at(284, 2)])
        self.channel_couping_mode = h2dig(last[at(286, 2)])
        self.bandwidth_limit_setting = h2dig(last[at(288, 2)])
        self.analog_attenaution_factor = h2dig(last[at(290, 2)])
        self.waveform_invert = h2dig(last[at(292, 2)])

        self.time_acquired_index = uint2(last[at(294, 2)])

        self.horizontal_trigger_time = float4(last[at(296, 4)])
        self.horizontal_processed_trigger_time = float4(last[at(300, 4)])
        self.recording_depth = uint4(last[at(304, 4)])  # h2dig?

        self.sampling_rate = float4(last[at(316, 4)])

        self.screen_coordinates_acquired_begin = int4(last[at(524, 4)])
        self.screen_coordinates_acquired_end = int4(last[at(528, 4)])

        self.waveform_data_interpolation_flag = uint4(last[at(540, 4)])  # h2dig?

        self.time_interval = float4(last[at(548, 4)])
        self.no_of_horizontal_offset = int4(last[at(552, 4)])

        self.realtime_voltage_index = np.empty(4, dtype=np.uint16)
        self.realtime_voltage_index[0] = uint2(last[at(768, 2)])
        self.realtime_voltage_index[1] = uint2(last[at(770, 2)])
        self.realtime_voltage_index[2] = uint2(last[at(772, 2)])
        self.realtime_voltage_index[3] = uint2(last[at(774, 2)])

        self.realtime_zero = np.empty(4, dtype=np.float32)
        self.realtime_zero[0] = float4(last[at(776, 4)])
        self.realtime_zero[1] = float4(last[at(780, 4)])
        self.realtime_zero[2] = float4(last[at(785, 4)])
        self.realtime_zero[3] = float4(last[at(788, 4)])

    def adc_to_volt(self, measure, channel):
        return (measure / 6400 - self.acquired_zero[channel]) * self.voltage_scale[
            self.acquired_voltage_index[channel]
        ]


class OscilloscopeDataProperty(OscilloscopeProperty):
    def __init__(self, data):
        super().__init__(data)
        self.len = self.n2

        last = self.data[-self.d :]

        # uint2(last[at(N1 + 10, 2)])
        if 1 <= self.n1 <= 4 and self.n2 > 0:
            self.waveform_channel = np.empty((self.n3, self.n1))
            self.waveform_adc = np.empty((self.n3, self.n1, self.n2))
            for i3 in range(self.n3):
                for i1 in range(self.n1):
                    self.waveform_channel[i3:i1] = uint2(
                        last[at(self.N1 + 12 + (self.n2 * 2 + 2) * i1 * i3, 2)]
                    ).squeeze()
                    self.waveform_adc[i3:i1] = int2(
                        last[
                            at(self.N1 + 14 + (self.n2 * 2 + 2) * i1 * i3, self.n2 * 2)
                        ]
                    )

        if hexl(last[at(self.N1 + self.N2 + 12, 4)]) != "0A0A0550".lower():
            raise OscilloscopeWrongFormat()

        # uint2(last[at(N1 + N2 + 16, 2)])
        # uint2(last[at(N1 + N2 + 18, n5 * 2)])
        if uint2(last[at(self.N1 + self.N2 + self.N3 + 16, 2)]) != self.dyn_check:
            raise OscilloscopeWrongFormat()

        if (
            hexl(last[at(self.N1 + self.N2 + self.N3 + 18, 8)])
            != "0906060905A0050A".lower()
        ):
            raise OscilloscopeWrongFormat()

    def time(self):
        """
        in us
        """
        return np.arange(0, self.len) * self.time_interval


class OscilloscopePreambleProperty(OscilloscopeProperty):
    def __init__(self, data):
        super().__init__(data)

    def time(self, slen):
        return np.arange(0, slen) * 1.0 / self.sampling_rate


class OscilloscopeRawDataProperty:
    def __init__(self, data):
        self.data = data
        ini = chr(self.data[0])
        if ini != "#":
            raise OscilloscopeWrongFormat()

        n = int(chr(self.data[1]))
        d = int(self.data[at(2, n)])
        if len(self.data[2 + n :]) != d:
            raise OscilloscopeWrongFormat()

        last = self.data[-d:]

        self.waveform_adc = int2(last)

        self.len = self.waveform_adc.size
