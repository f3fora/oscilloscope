from inspect import getfullargspec
from time import sleep


class SetNoValueError(Exception):
    pass


class GetNoValueError(Exception):
    pass


def __inspect_kwargs(fn, **kwargs):
    return {k: v for k, v in kwargs.items() if k in list(getfullargspec(fn))[0]}


def _set(self, *values):
    if len(values) != self._arguments:
        raise SetNoValueError()

    return self._instrument.write(f"{self._cmd} {','.join([str(v) for v in values])}")


def _get(self, *values, **kwargs):
    if len(values) != self._get_arguments:
        raise GetNoValueError()

    w = f" {','.join([str(v) for v in values])}" if len(values) > 0 else ""

    return self._instrument.query(f"{self._cmd}?{w}", **kwargs)


def _get_raw(self, delay=0.0, **kwargs):
    write_kwargs = __inspect_kwargs(self._instrument.write, **kwargs)
    self._instrument.write(f"{self._cmd}?", **write_kwargs)
    read_kwargs = __inspect_kwargs(self._instrument.read_raw, **kwargs)
    sleep(delay)
    return self._instrument.read_raw(**read_kwargs)


def _get_bytes(self, no_bytes, delay=0.0, **kwargs):
    write_kwargs = __inspect_kwargs(self._instrument.write, **kwargs)
    self._instrument.write(f"{self._cmd}?", **write_kwargs)
    read_kwargs = __inspect_kwargs(self._instrument.read_bytes, **kwargs)
    sleep(delay)
    return self._instrument.read_bytes(no_bytes, **read_kwargs)


def _get_computed(self, **kwargs):
    if self.conversion is None:
        return self.get_raw(**kwargs)

    return self.conversion(self.get_raw(**kwargs))


def _do(self):
    return self._instrument.write(self._cmd)


def _values(self):
    return self._values


def _show(self):
    return self._cmd


def initProperty(cls):
    def initProperty(*args, **kwargs):
        obj = cls(*args, **kwargs)

        class Property:
            def __init__(self):
                self._cmd = obj.cmd
                self._instrument = obj.instrument

                if (obj.set or obj.get or obj.get_bytes) and obj.values is not None:
                    self._values = obj.values

                if obj.set:
                    self._arguments = obj.arguments

                if obj.get:
                    self._get_arguments = obj.get_arguments

                if obj.get_bytes:
                    self.conversion = None

            __str__ = _show
            __repr__ = _show

        if (obj.set or obj.get or obj.get_bytes) and obj.values is not None:
            setattr(Property, "values", property(_values))

        if obj.set:
            setattr(Property, "set", _set)

        if obj.get:
            setattr(Property, "get", _get)

        if obj.get_bytes:
            setattr(Property, "get_raw", _get_raw)
            setattr(Property, "get_bytes", _get_bytes)
            setattr(Property, "get", _get_computed)

        if obj.do:
            setattr(Property, "do", _do)

        return Property()

    return initProperty


@initProperty
class Property:
    def __init__(
        self,
        instrument,
        cmd,
        values=None,
        set=False,
        get=False,
        get_bytes=False,
        do=False,
        arguments=1,  # set
        get_arguments=0,
    ):
        self.instrument = instrument
        self.cmd = cmd
        self.values = values
        self.set = set
        self.get = get
        self.get_bytes = get_bytes
        self.do = do
        self.arguments = arguments
        self.get_arguments = get_arguments
