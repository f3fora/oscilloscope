import numpy as np
import pyvisa as pv

from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation
from matplotlib.widgets import Button

from core.oscilloscope import Oscilloscope, model
from utils.unit_converts import to_num
from config import config


def main():
    __rm = pv.ResourceManager()
    __res = __rm.list_resources()

    if not __res:
        raise Exception()

    global inst
    for r in __res:
        inst = Oscilloscope(__rm.open_resource(r))
        if inst.idn.get().replace("\n", "").lower() != model.lower():
            inst._instrument.close()
        else:
            break

    else:
        raise Exception()

    inst.ch1_offset.set(0)
    inst.ch2_offset.set(0)

    config(inst)

    global flag
    flag = 1

    global y_min, y_max, x_min, x_max
    y_min, y_max, x_min, x_max = 0, 0, 0, 0

    global first
    first = True

    f = plt.figure()
    gs = f.add_gridspec(
        2,
        2,
        height_ratios=[10, 1],
        width_ratios=[1, 10],
        hspace=0.5,
        wspace=0.5,
    )

    axs = [None] * 9
    ln = [None] * 2

    axs[0] = f.add_subplot(gs[0, 0:2])
    axs[1] = f.add_subplot(gs[1, 0])

    (ln[0],) = axs[0].plot([], [])
    (ln[1],) = axs[0].plot([], [])

    b_pp = Button(axs[1], label="Pause")

    axs[0].set_ylabel("V [V]")
    axs[0].set_xlabel("time [ns]")
    axs[0].ticklabel_format(axis="x", scilimits=(-3, 4))

    def toggle(_):
        global flag
        if flag == 1:
            b_pp.label.set_text("Play")
            flag = 2
        elif flag == 2:
            b_pp.label.set_text("Pause")
            flag = 1

    def reset():
        global flag
        flag = False
        inst.rst.do()
        inst.stop.do()
        inst._instrument.close()

    b_pp.on_clicked(toggle)

    def animate(i):
        if i == 2:
            return

        v = []
        try:
            x = int(to_num(inst.acquire_depmem.get()))
            inst.waveform_begin.set("CH1")
            wp = inst.waveform_preamble.get()
            inst.waveform_range.set(0, x)
            v.append(inst.waveform_fetch.get())
            inst.waveform_begin.set("CH2")
            inst.waveform_range.set(0, x)
            v.append(inst.waveform_fetch.get())
        except:
            pass
        finally:
            inst.waveform_end.do()

        if v:
            _y_min = []
            _y_max = []
            for j, w in enumerate(v):
                if wp.channel_working_status[j]:
                    time = wp.time(w.len)
                    ln[j].set_xdata(time)
                    ln[j].set_ydata(wp.adc_to_volt(w.waveform_adc, j))
                    _y_min.append(wp.adc_to_volt(wp.adc_minimum, j))
                    _y_max.append(wp.adc_to_volt(wp.adc_maximum, j))
                else:
                    ln[j].set_xdata([])
                    ln[j].set_ydata([])

            global y_min, y_max, x_min, x_max

            y_min = np.array(_y_min).min()
            y_max = np.array(_y_max).max()
            x_min = 0
            x_max = wp.time_interval * v[0].len

            global first
            if first:
                axs[0].set_ylim(y_min, y_max)
                axs[0].set_xlim(x_min, x_max)
                first = False

    def gen():
        global flag
        while flag:
            yield flag

    # create animation using the animate() function
    animation = FuncAnimation(
        f, animate, frames=gen(), interval=50, blit=False, repeat=False
    )

    plt.show(block=True)
    # reset()


if __name__ == "__main__":
    main()
