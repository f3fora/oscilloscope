import re

__suffix = {"n": 1e-9, "u": 1e-6, "m": 1e-3, "K": 1e3, "M": 1e6}
__numeric_const_pattern = r"([+-]?\d*\.?\d+(?:[eE][-+]?\d+)?)"


def to_num(string):
    rx = re.compile(__numeric_const_pattern)
    list_num = rx.findall(string)
    if len(list_num) == 0:
        return 0.0

    num = float(next(iter(list_num)))
    list_suf = set(c for c in string if c in __suffix.keys())
    if len(list_suf) == 0:
        suf = 1.0
    else:
        suf = __suffix[next(iter(list_suf))]

    return num * suf
